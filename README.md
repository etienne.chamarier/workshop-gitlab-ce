# workshop-gitlab-ce

## To-do

- [ ] Step 1: create a new **LXC container** (CT) on **NODE01** (srv proxmox wild code school)
  - Template: Debian 12
  - Storage: "local-nvme-datas" with 4gb
  - CPU: 2 Cores
  - Memory: 4gb
  - Network:
    - interface: vmbr3
    - IPv4/CIDR: 192.168.1.`<lxc_id>`/24
    - Gateway (IPv4): 192.168.1.254
- [ ] Step 2: follow the step [describes here](https://www.linuxtechi.com/how-to-install-gitlab-on-debian/) to install Gitlab CE with apt and specific Gitlab repository
- [ ] Step 3: create an ansible's role that will install Gitlab CE on a remote managed node
- [ ] Step 4: create the playbook that allows the play of these ansible's roles
- [ ] Step 5: Fill in the "Getting Started" part with instructions on the manual installation of Gitlab CE
- [ ] Step 6: Fill in the "Getting Started" part with instructions on the installation's of Gitlab CE with your Ansible's role
- [ ] Step 7: push your project to gitlab or github and make the repository accessible to your trainer

## Getting started

@todo
